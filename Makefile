all:

images: consumer-image producer-image retriever-image digger-image

push-images: push-consumer-image push-producer-image push-retriever-image push-digger-image

consumer-image:
	cd consumer; docker build -t registry.gitlab.com/africamediaonline/dockers/intradoc-consumer .

push-consumer-image:
	docker push registry.gitlab.com/africamediaonline/dockers/intradoc-consumer

producer-image:
	cd producer; docker build -t registry.gitlab.com/africamediaonline/dockers/intradoc-producer .

push-producer-image:
	docker push registry.gitlab.com/africamediaonline/dockers/intradoc-producer

retriever-image:
	cd retriever; docker build -t registry.gitlab.com/africamediaonline/dockers/intradoc-retriever .

push-retriever-image:
	docker push registry.gitlab.com/africamediaonline/dockers/intradoc-retriever


digger-image:
	cd digger; docker build -t registry.gitlab.com/africamediaonline/dockers/intradoc-digger .

push-digger-image:
	docker push registry.gitlab.com/africamediaonline/dockers/intradoc-digger
