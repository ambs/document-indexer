#!/usr/bin/env python3
import sys

import pika
import json
import os, os.path
from elasticsearch6 import Elasticsearch, helpers
import pytesseract as ocr
from pytesseract import Output
import cv2
import re
import smtplib
from email.message import EmailMessage
import logging


def get_boxes_ocr(imgloc, cid):
    d = ocr.image_to_data(imgloc, output_type=Output.DICT)
    i = 0
    hits = []
    for key in d['text']:
        if not re.search(r'^\s*$', key) and not len(key) < 2:
            word = str.lower(key)
            word = re.sub(r'\W+$', "", word)
            word = re.sub(r'\u2019', "'", word)
            if len(word) > 2:
                hits.append({"word": word,
                             "canvas_id": cid,
                             "bbox": [d[x][i] for x in ['left', 'top', 'width', 'height']]
                             })
        i = i + 1
    return hits


def get_ocr(imgloc, cid):
    return ocr.image_to_string(imgloc)


def save_ngrams(imgloc, cid, iid, pid):
    text = get_ocr(imgloc, cid)

    doc = {
        'text': text if bool(text) else "",
        'canvas_id': cid,
        'document_id': iid,
        'id': pid
    }

    actions = [{
        "_index": "ngrams",
        "_id": pid,
        "_type": "_doc",
        "_source": doc
    }]

    try:
        helpers.bulk(es, actions)

    except Exception as e:
        logging.error("Failing bulk " + str(e))


def save_boxes(imgloc, cid, iid, pid):
    hits = get_boxes_ocr(imgloc, cid)

    doc = {
        'hits': hits if bool(hits) else [],
        'canvas_id': cid,
        'document_id': iid,
        'id': pid
    }

    actions = [{
        "_index": "pages",
        "_id": pid,
        "_type": "_doc",
        "_source": doc
    }]

    try:
        helpers.bulk(es, actions)

    except Exception as e:
        logging.error("Failing bulk " + str(e))


def callback(ch, method, properties, body):
    if es_not_runnig():
        response = {'Impossible to connect to ElasticSearch'}
        return print(response)

    ch.basic_ack(delivery_tag=method.delivery_tag)

    data = json.loads(body)
    iid = (data['iid'])
    pid = (data['pid'])
    cid = (data['cid'])

    path = '/Imagedata/' + data['file']

    if os.path.exists(path):
        logging.warning("ocr %s" % path)
        imgloc = cv2.imread(path)

        save_boxes(imgloc, cid, iid, pid)
        save_ngrams(imgloc, cid, iid, pid)

        try:
            os.remove("%s" % path)
        except Exception as e:
            logging.error("failing to remove file %s" % path)
    else:
        logging.error("No such file " + path)


def es_not_runnig():
    return not es.ping()


if __name__ == "__main__":
    host = os.environ['HOSTNAME']
    pid = os.getpid()
    es = Elasticsearch(hosts='es')

    dictionary = set(line.strip() for line in open('dicts/en.dic'))

    logging.basicConfig(
        stream=sys.stdout,
        format='[Consumer|%(asctime)s] %(levelname)s %(message)s',
        level=logging.WARNING
    )

    logging.info("Starting consumer")

    credentials = pika.PlainCredentials(
        username='guest',
        password='guest'
    )
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(
            host='rabbit',
            credentials=credentials,
            heartbeat=0
            # ,
            # blocked_connection_timeout=300
        )
    )

    if not es.indices.exists(index="pages"):
        index_config = {
            "mappings": {
                "_doc": {
                    "properties": {
                        "hits": {
                            "type": "nested",
                            "properties": {
                                "word": {"type": "keyword"}
                            }
                        }
                    }
                }
            }
        }
        es.indices.create(index='pages', body=index_config)

    if not es.indices.exists(index="ngrams"):
        index_config = {
            "settings": {
                "analysis": {
                    "analyzer": {
                        "my_lc_analyzer": {
                            "type": "custom",
                            "tokenizer": "standard",
                            "filter": ["lowercase", "asciifolding"]
                        }}}},
            "mappings": {
                "_doc": {
                    "properties": {
                        "text": {"type": "text",
                                 "analyzer": "my_lc_analyzer"}}}}}
        es.indices.create(index='ngrams', body=index_config)

    channel = connection.channel()
    channel.queue_declare(queue='ocr', durable=True)
    channel.basic_consume(on_message_callback=callback, queue='ocr')
    channel.start_consuming()
