
# IIIF Notes

## Indexing

1. Receives a manifest (see 01 file)
2. Downloads images
3. Indexes pages
4. Returns a manifest (see 02 file)

## Suggestion Autocomplete

1. Using the URL in the service section:

```
# example for prefix 'cric'
https://mathmos.dlc-services.africamediaonline.com/search/aHR0cDovLzI0NTk4MWVjNWE2NDcxYTcyYjM1MTAxNWUzZDVmMGYz/autocomplete/oa?q=cric
```

Returns a json (see 03 file)

## Hits retrieval

1. Using the URL in the autocomplete answer:

```
https://mathmos.dlc-services.africamediaonline.com/search/aHR0cDovLzI0NTk4MWVjNWE2NDcxYTcyYjM1MTAxNWUzZDVmMGYz/search/oa?q=cricket
```

(see file 04)
