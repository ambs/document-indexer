#!/usr/bin/env python3
# -*- coding: utf-8 -*- 3

import os
import json
import sys
import uuid
from elasticsearch import Elasticsearch, RequestError, exceptions
from flask import Flask, request  # , jsonify, make_response
from flask_cors import CORS

import logging

app = Flask(__name__)
CORS(app)

eshost = os.environ['IDS_ELASTICSEARCH_HOST'] if 'IDS_ELASTICSEARCH_HOST' in os.environ else "https://ip-10-1-66-235.af-south-1.compute.internal"
esport = os.environ['IDS_ELASTICSEARCH_PORT'] if 'IDS_ELASTICSEARCH_PORT' in os.environ else 9200

host = os.environ['IDS_DIGGER_HOST'] if 'IDS_DIGGER_PORT' in os.environ else "https://ids.africamediaonline.com"

es = Elasticsearch(hosts=f"{eshost}:{esport}", 
                   basic_auth=('elastic','SkLWuEBOifAw6Nx7xYLn'),
                   verify_certs=False)


# From here: https://stackoverflow.com/questions/952914/how-to-make-a-flat-list-out-of-list-of-lists
def _flatten(flist):
    return [item for sublist in flist for item in sublist]


def get_counts(metadata, index, verbose=False):
    images = metadata['images']
    total_count = len(images)
    missing = []
    ready = 0
    for image in images:
        try:
            ans = es.get(index=index, id=image['pid'])
            if ans['_source']:
                ready = ready + 1
        except exceptions.NotFoundError:
            if verbose:
                missing.append(image['uri'])
            pass
        except Exception as e:
            print(e)
            pass
    ans = {"total": total_count, "ready": ready}
    if verbose:
        ans['missing'] = missing
    return ans


def get_uuid():
    return uuid.uuid4().urn




@app.route('/raw-page/<pid>', methods=['GET'])
def get_page(pid):
    if es.ping():
        page = es.get(index="pages", id=pid)
        if page['_source']:
            return page['_source'], 200
        else:
            return {"NOK": "Document not found"}, 200
    else:
        return {"NOK": "ElasticSearch not answering"}, 400


@app.route('/raw-ngrams/<pid>', methods=['GET'])
def get_ngrams(pid):
    if es.ping():
        page = es.get(index="ngrams", id=pid)
        if page['_source']:
            return page['_source'], 200
        else:
            return {"NOK": "Document not found"}, 200
    else:
        return {"NOK": "ElasticSearch not answering"}, 400


@app.route('/raw-metadata/<iid>', methods=['GET'])
def get_metadata(iid):
    if es.ping():
        page = es.get(index="metadata", id=iid)
        if page['_source']:
            return page['_source'], 200
        else:
            return {"NOK": "Document not found"}, 200
    else:
        return {"NOK": "ElasticSearch not answering"}, 400


@app.route('/status/<manuscriptid>', methods=['GET'])
def get_status(manuscriptid):
    verbose = request.args.get('verbose') or False
    if es.ping():
        try:
            metadata = es.get(index="metadata", id=manuscriptid)
            if "_source" in metadata:
                countsP = get_counts(metadata['_source'], "pages", verbose=verbose)
                countsN = get_counts(metadata['_source'], "ngrams", verbose=verbose)
                ans = {
                    "id": manuscriptid,
                    "pages": {
                        "total": countsP['total'], "ready": countsP['ready']
                    },
                    "ngrams": {
                        "total": countsN['total'], "ready": countsN['ready']
                    }
                }
                if verbose:  # and countsP['ready'] != 0:
                    ans['pages']['missing'] = countsP['missing']
                if verbose:  # and countsN['ready'] != 0:
                    ans['ngrams']['missing'] = countsN['missing']
                return ans, 200
            else:
                return {"NOK": "Document not found"}, 200
        except exceptions.NotFoundError:
            return {"NOK": "Document not found"}, 200
    else:
        return {"NOK": "ElasticSearch not answering"}, 400


@app.route('/manifest/<manuscript_id>', methods=['GET'])
def get_manifest(manuscript_id):
    if es.ping():
        metadata = es.get(index="metadata", id=manuscript_id)

        if "_source" in metadata:

            if not ('ready' in metadata['_source']) or not (metadata['_source']['ready']):
                countsP = get_counts(metadata['_source'], "pages")
                countsN = get_counts(metadata['_source'], "ngrams")
                metadata['_source']['ready'] = countsP['ready'] == countsP['total'] and \
                                               countsN['ready'] == countsN['total']
                es.index(index='metadata', id=manuscript_id, body=metadata['_source'])

            if metadata['_source']['ready']:
                try:
                    fh = open("/Manifests/%s.json" % manuscript_id, "r")
                    manifest = json.load(fh)
                    manifest['service'] = [{
                        '@context': "http://iiif.io/api/search/1/context.json",
                        "@id": "%s/search/%s" % (host, manuscript_id),
                        "label": "Search within this manifest",
                        "profile": "http://iiif.io/api/search/0/search",
                        "service": {
                            "@id": "%s/auto-complete/%s" % (host, manuscript_id),
                            "label": "Get suggested words in this manifest",
                            "profile": "http://iiif.io/api/search/0/autocomplete"

                        }
                    }]

                    return manifest, 200
                except Exception:
                    return {"NOK": "Original Manifest not found!!"}

            else:
                return {"NOK": "Document not ready"}, 400
        else:
            return {"NOK": "Document not found"}, 400
    else:
        return {"NOK": "ElasticSearch not answering"}, 400


@app.route('/auto-complete/<manuscript_id>', methods=['GET'])
def auto_complete(manuscript_id):
    term = request.args.get('q').lower()

    if es.indices.exists(index="metadata"):
        try:
            ans = es.get(index='metadata', id=manuscript_id)
            if "_source" in ans and "ready" in ans['_source'] and ans["_source"]["ready"]:
                hits = es.search(
                    index="pages",
                    body={
                        "size": 0,
                        "_source": False,
                        "aggs": {
                            "nested_hits": {
                                "nested": {
                                    "path": "hits"
                                },
                                "aggs": {
                                    "nested_prefix": {
                                        "filter": {"prefix": {"hits.word": {"value": term}}},
                                        "aggs": {"words": {"terms": {"field": "hits.word"}}}
                                    },
                                },
                            }
                        },
                        "query": {
                            "bool": {
                                "must": [
                                    {
                                        "match": {"document_id": manuscript_id}
                                    }
                                ],
                            }
                        }
                    }
                )
                terms = []
                if "aggregations" in hits and \
                        "nested_hits" in hits["aggregations"]:
                    buckets = hits['aggregations']['nested_hits']['nested_prefix']['words']['buckets']
                    for bucket in buckets:
                        terms.append({
                            "match": bucket["key"],
                            "url": "%s/search/%s?q=%s" % (host, manuscript_id, bucket['key'])
                        })

                output = {
                    # "@ES": hits,
                    "@context": "http://iiif.io/api/search/0/context.json",
                    "@id": "%s/auto-complete/%s?q=%s" % (host, manuscript_id, term),
                    "@type": "search:TermList",
                    "terms": terms
                }
                return output, 200
            else:
                return {"NOK": "Manifest not found or not ready", "ans": ans}, 400
        except RequestError as rerr:
            return {"NOK": "Error contacting ES", "Msg": format(rerr)}, 400
        except Exception as exc:
            return {"NOK": "Error contacting ES", "Msg": format(exc)}, 400


@app.route('/search/<manuscript_id>', methods=['GET'])
def search(manuscript_id):
    term = request.args.get('q').lower()

    terms = [x for x in term.split() if len(x) > 2]
    is_multiword = len(terms) > 1

    if es.indices.exists(index="metadata"):
        try:
            ans = es.get(index='metadata', id=manuscript_id)
            if "_source" in ans and "ready" in ans['_source'] and ans["_source"]["ready"]:

                mw_hits = {}
                if is_multiword:
                    page_hits = es.search(
                        index="ngrams",
                        body={
                            "size": 10000,
                            "query": {
                                "match_phrase": {
                                    "text": term
                                }
                            }
                        }
                    )
                    if 'hits' in page_hits and 'hits' in page_hits['hits']:
                        for hit in page_hits['hits']['hits']:
                            mw_hits[hit['_id']] = 1

                hits_by_canvas = {}
                for single_word in terms:
                    hits = es.search(
                        index="pages",
                        body={
                            "size": 10000,
                            "_source": False,
                            "query": {
                                "bool": {
                                    "must": [
                                        {
                                            "nested": {
                                                "path": "hits",
                                                "query": {
                                                    "bool": {
                                                        "must": [
                                                            {"match": {"hits.word": single_word}}
                                                        ]
                                                    }
                                                },
                                                "inner_hits": {}
                                            }
                                        },
                                        {
                                            "match": {"document_id": manuscript_id}
                                        }
                                    ],
                                }
                            }
                        }
                    )

                    if "hits" in hits and "hits" in hits['hits']:
                        for page in hits['hits']['hits']:
                            if 'inner_hits' in page and \
                                    'hits' in page['inner_hits'] and \
                                    'hits' in page['inner_hits']['hits'] and \
                                    'hits' in page['inner_hits']['hits']['hits']:
                                for hit in page['inner_hits']['hits']['hits']['hits']:
                                    page_id = hit['_id']
                                    if '_source' in hit:
                                        bbox = hit['_source']['bbox']
                                        word = hit['_source']['word']
                                        canvas_id = hit['_source']['canvas_id']
                                        hits_by_canvas.setdefault(page_id, []).append({"@id": get_uuid(),
                                            "@type": "oa:Annotation",
                                            "motivation": "sc:painting",
                                            "on": "%s#xywh=%d,%d,%d,%d" % (canvas_id, bbox[0], bbox[1], bbox[2], bbox[3]),
                                            "resource": {
                                                "@type": "cnt:ContentAsText",
                                                "chars": word
                                            }
                                        })

                output_hits = []
                if is_multiword:
                    for mw_hit in mw_hits.keys():
                        output_hits.extend(hits_by_canvas[mw_hit]) if mw_hit in hits_by_canvas else ...
                else:
                    output_hits = _flatten(hits_by_canvas.values())

                output = {
                    "resources": output_hits,
                    "@context": "http://iiif.io/api/presentation/2/context.json",
                    "@id": "%s/search/%s?q=%s" % (host, manuscript_id, term),
                    "@type": "sc:AnnotationList",
                }
                return output, 200
            else:
                return {"NOK": "Manifest not found or not ready"}, 400
        except RequestError as rerr:
            return {"NOK": "Error contacting ES", "Msg": format(rerr)}, 400
        except Exception as exc:
            return {"NOK": "Error contacting ES", "Msg": format(exc)}, 400


if __name__ == "__main__":
    port = os.environ['IDS_DIGGER_PORT'] if 'IDS_DIGGER_PORT' in os.environ else 3000
    app.run(debug=True, host="0.0.0.0", port=port)
