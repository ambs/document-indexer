#!/usr/bin/env python3
import logging
import sys

import pika
import json
import urllib.request
import re
import os
import glob

MAX_FILES_WAITING = 1000
DOWNLOAD_FOLDER = "/Imagedata"


def clean_download_folder():
    for file in glob.glob0(DOWNLOAD_FOLDER, "*.*"):
        os.remove(file)


def wait_if_needed():
    if len(os.listdir(DOWNLOAD_FOLDER)) > MAX_FILES_WAITING:
        logging.warning('Sleep mode')
        connection.sleep(5 * 60)   # five minutes


def callback(ch, method, properties, body):
    data = json.loads(body)
    uri = data['uri']
    ext_search = re.search(r"\.([^.]+)$", uri)
    ext = ext_search.group(1) if ext_search else ""
    data['file'] = '%s.%s' % (data['pid'], ext)
    out = "%s/%s" % (DOWNLOAD_FOLDER, data['file'])
    try:
        urllib.request.urlretrieve(uri, out)
        ocrChannel.basic_publish(exchange='', routing_key='ocr', body=json.dumps(data))
        logging.warning("Stored %s." % uri)
    except urllib.error.HTTPError as e:
        logging.warning('Failed %s => %s' % (uri, str(e)))
    finally:
        ch.basic_ack(delivery_tag=method.delivery_tag)
    wait_if_needed()


logging.basicConfig(
    stream=sys.stdout,
    format='[Retriever|%(asctime)s] %(levelname)s: %(message)s',
    level=logging.WARNING,
)

if __name__ == "__main__":
    clean_download_folder()

    credentials = pika.PlainCredentials('guest', 'guest')
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='rabbit', credentials=credentials))

    ocrChannel = connection.channel()
    ocrChannel.queue_declare(queue='ocr', durable=True)

    downloadChannel = connection.channel()

    downloadChannel.queue_declare(queue='download', durable=True)
    downloadChannel.basic_consume(on_message_callback=callback, queue='download', auto_ack=False)
    downloadChannel.start_consuming()
