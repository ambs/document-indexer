#!/usr/bin/env python3
# -*- coding: utf-8 -*- 3
from flask import Flask, request
import pika
import json
import sys
import hashlib
from elasticsearch6 import Elasticsearch
import logging


app = Flask(__name__)


class RabbitConnection:
    def __init__(self):
        self._credentials = pika.PlainCredentials(username='guest', password='guest')
        self._connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                host='rabbit',
                credentials=self._credentials,
                heartbeat=600,
                blocked_connection_timeout=300
            )
        )

        self._channel = self._connection.channel()
        self._channel.queue_declare(queue='download', durable=True)

    def publish(self, message):
        self._channel.basic_publish(exchange='', routing_key='download', body=message)
        return True

    def close(self):
        self._connection.close()


def manifest_exists(iid):
    if es.ping():
        return es.exists(index="metadata", id=iid, doc_type="_doc")
    else:
        logging.warning("Lost ES connection while trying to check manifest existence")
        raise Exception("ES Connection Lost")


def page_exists(index, pid):
    if es.ping():
        return es.exists(index=index, id=pid, doc_type="_doc")
    else:
        logging.warning("Lost ES connection while trying to check page existence")
        raise Exception("ES Connection Lost")


def delete_manifest(iid,delete_pages=True):
    if es.ping():

        # Get metadata, and delete it
        metadata = es.get(index="metadata", id=iid, doc_type="_doc")
        try:
            es.delete(index="metadata", doc_type="_doc", id=iid)
        except Exception as e:
            logging.error("Unable to delete metadata doc %s (%s)" % (iid, str(e)))

        # if we need to delete pages
        if delete_pages and '_source' in metadata and 'images' in metadata['_source']:
            for page in metadata['_source']['images']:
                if page_exists("pages", page['pid']):
                    try:
                        es.delete(index="pages", doc_type="_doc", id=page['pid'])
                    except Exception as e:
                        logging.error("Unable to delete page doc %s (%s)" % (page['pid'], str(e)))
                if page_exists("ngrams", page['pid']):
                    try:
                        es.delete(index="ngrams", doc_type="_doc", id=page['pid'])
                    except Exception as e:
                        logging.error("Unable to delete ngrams doc %s (%s)" % (page['pid'], str(e)))


    else:
        logging.warning("Lost ES connection while trying to delete manifest/pages")
        raise Exception("ES Connection Lost")


class Manifest:
    def __init__(self, body, missing=False):
        self._json = body

        if not('@id' in self._json) or not('sequences' in self._json):
            logging.warning("Not a valid manifest file!?") 
            raise Exception("Not a valid manifest file!?") 

        self._id = self._json['@id']
        self._iid = hashlib.md5(self._id.encode('utf-8')).hexdigest()

        if not missing and manifest_exists(self._iid):
            delete_manifest(self._iid)

        logging.warning("Checked existence")

        total_images = 0
        images = []
        sequences_count = 0
        for sequence in self._json['sequences']:
            if not('canvases' in sequence):
                raise Exception("Not a valid manifest file")
            canvas_count = 0
            for canvas in sequence['canvases']:
                if not('images' in canvas) or not('@id' in canvas):
                    raise Exception("Not a valid manifest file")
                canvas_id = canvas['@id']
                images_count = 0
                for image in canvas['images']:
                    if not('resource' in image) or \
                       not('service'  in image['resource']):
                        raise Exception("Not a valid manifest file")

                    pid = "%s-%d-%d-%d" % (self._iid, sequences_count, canvas_count, images_count)
                    uri = image['resource']['@id']
                    images.append({"pid": pid, "uri": uri})

                    if not missing or not page_exists("pages", pid) or not page_exists("ngrams", pid):
                        connection = RabbitConnection()
                        result = connection.publish(json.dumps({
                            "iid": self._iid,
                            "pid": pid,
                            "uri": uri,
                            "cid": canvas_id,
                        }))
                        connection.close()

                    images_count = images_count + 1
                    total_images = total_images + 1
                canvas_count = canvas_count + 1
            sequences_count = sequences_count + 1

        if es.ping():
            if missing:
                delete_manifest(self._iid, delete_pages=False)
            res = es.index(index='metadata',
                           id=self._iid,
                           doc_type="_doc",
                           body={
                            "manifest_id": self._id,
                            "id": self._iid,
                            "image_count": total_images,
                            "images": images
                           })
            print('Saved successfully into ElasticSearch')

    def store(self):
        outfile = open("/Manifests/%s.json" % self._iid, "w")
        json.dump(self._json, outfile)
        outfile.close()
        return self._iid


@app.route('/manifest', methods=['POST'])
def post_manifest():
    if not es.ping():
        return {"NOK": "Impossible to connect to ElasticSearch"}, 400
    # print(request.json)
    # json_data = json.loads(request.data)
    json_data = request.json
    try:
        manifest = Manifest(json_data, missing=True if request.args.get('continue') else False)
        iid = manifest.store()
        return {"OK": "Stored", "id": iid}, 200
    except Exception as exception:
        return {"NOK": exception.args[0]}, 200


if __name__ == "__main__":
    es = Elasticsearch(hosts='es')
    logging.basicConfig(
        stream=sys.stdout,
        format='[Producer|%(asctime)s] %(levelname)s:%(message)s',
        level=logging.WARNING,
    )

    logging.info("Starting Producer")
    app.debug = True
    app.run(host="0.0.0.0")
