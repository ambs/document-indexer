# IntraDoc Search Engine

## Services Architecture

The IntraDoc Search Engine has four main services, and two main storage engines.

The services are:

 * **Producer**
 
   Responsible for receiving an IIIF manifest, downloading the referred images,
   and putting identifiers on a queue (see RabbitMQ later) to be processed.

 * **Retriever**
 
   The retriever gets tasks from the RabbitMQ, downloads images, and sends an OCR task
   to another queue. This service tries to reduce the amount of used disk space, keeping 
   up to 1000 downloaded images as a maximum.
   
 * **Consumer**
 
   Responsible for the OCR task. Retrieves identifiers from the queue (RabbitMQ),
   passes the images through Tesseract OCR, and then stores in the main database
   (ElasticSearch) the obtained data.
      
 * **Digger**
 
   This is que search endpoint, that answers IIIF requests for auto completion or
   search, querying the ElasticSearch storage engine.
   
The two storage engines are:

 * **ElasticSearch**
 
   Used to store each document page information, with word hits
   
 * **RabbitMQ**
 
   A simple queue used to store pages that needs to be processed.
   
## Workflow

 1. The **Producer** receives a POST with a IIIF manifest.

    1. An internal ID (IID) for that manifest is computed (md5 from
       the top level @id)

    2. It is created an entry in the ES index for metadata. For
       instance, `/metadata/_docs/<IID>` has
    
       ```json
       {
        "manifest_id" : "theOriginalManifest@id",
        "page_count" : 227,
        "images" : [ { "pid":"page_id1", "uri":"http://"}, {"pid":"id2", "uri":"http://..." }],
        "id" : "IID"
       }
       ```

    3. The received manifest is saved in the filesystem (use the IID)

    4. For each page of the manifest, send to the **Retriever** queue the necessary
       information: IID, page number, URI to image.

 2. The **Retriever** dequeues images from its queue, download the image to a shared
    folder, and enqueues the data into the **Consumer** queue.

 2. The **Consumer** gets a page information and:
 
    1. Runs it through Tesseract
    
    2. Lowercases all the words
    
    3. Saves a document in ES for each page. As identifier, use the document IID 
       and the page number to compute a page identifier. Each record is stored as
       `/pages/_docs/<PID>` (where PID is the page ID)
       
       ```json
       {
           "id" : "PID",
           "document_id" : "IID",
           "hits" : [
              { "word": "the",   "bbox":  [10,35,200,354] },
              { "word": "brown", "bbox":  [10,35,200,354] },
              { "word": "fox",   "bbox":  [10,35,200,354] },
              { "word": "jumps", "bbox":  [10,35,200,354] },
              { "word": "over",  "bbox":  [10,35,200,354] } 
           ]
       }
       ```
       
       Note that empty pages should be stored with an empty `hits` lists

 3. The digger, answers to the following routes:
 
    1. `/manifest/<IID>` to obtain the modified manifest
    2. `/status/<IID>` to obtain current indexing status. 
       An optional `?verbose=1` parameter might be added to
       receive a detailed log of the missing pages (for already
       started, non finished documents).
    3. `/autocomplete/<IID>?q=query` to obtain list of completions
    4. `/search/<IID>?q=query` to obtain list of hits


## Basic Docker Documentation

 - To start up everything `docker-compose up -d`
 - Individual containers can be started as well: `docker-compose up -d producer`
 - It is possible to request the container to be recreated (`--force-recreate`) or the image rebuilt (`--build`)
 - To get a list of the running containers, and their basic status: `docker ps`
 - To see logs, `docker logs -f`. You can specify a container name as well.
 - To run bash, and see the contents of a conter, use `docker exec -it <CONTAINER_ID> bash`

**NOTE:** Do not forget to set `sysctl -w vm.max_map_count=262144` in your host, or ElasticSearch will keep dying. You can also add this into `/etc/sysctl.conf` to preserve the change across reboots.

## Authors

 * Miguel Tinoco
 * Rui Meira
 * Alberto Simões
 