#!/usr/bin/env python3
# -*- coding: utf-8 -*- 3
import pytesseract as ocr
from pytesseract import Output
import cv2
import json
import re
import sys

imgloc = cv2.imread(sys.argv[1])
d = ocr.image_to_data(imgloc, output_type=Output.DICT)
i = 0
hits = []
for key in d['text']:
    if not re.search(r'^\s*$', key) and not len(key) < 2:
        word = str.lower(key)
        word = re.sub(r'\W+$', "", word)
        word = re.sub(r'\u2019', "'", word)
        if len(word) > 2:
            hits.append({word: [d[x][i] for x in ['left', 'top', 'width', 'height']]})

    i = i + 1

for k in hits:
    print(repr(k).replace('{',r'').replace('}',r'').replace('[',r'').replace(']',r''))

