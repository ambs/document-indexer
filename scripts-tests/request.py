#!/usr/bin/env python3
# -*- coding: utf-8 -*- 3
import os
import os.path
import json
import requests
import argparse
import urllib3.request
import shutil


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--file", help="Need a valid path of an document.", type=str)
    parser.add_argument("--id", help="ID of the document.", type=str)
    parser.add_argument(
        "--json", help="Need a valid Json path of a .json file.", type=str)
    args = parser.parse_args()

    if args.json != None:
        if os.path.exists(args.json):
            try:
                with open(args.json) as json_file:
                    data = json.load(json_file)
                    doc = None
                    json_body = []
                    urls_list = []
                    for i in data['sequences']:
                        for canva in i['canvases']:
                            for image in canva['images']:
                                json_body.append(image['resource']['@id'])
                                # urls_list.append(image['resource']['@id'])

                    # Used to get the url in the urls_list and download all of the images to mypath
                    # c = urllib3.PoolManager()
                    # for i, url in enumerate(urls_list):
                    #     mypath = '../Image'
                    #     file_name = url.replace('https://', '').replace('/','.')
                    #     full_file_name = os.path.join(mypath,file_name)
                    #     with c.request('GET',url, preload_content=False) as resp, open(full_file_name, 'wb') as out_file:
                    #         shutil.copyfileobj(resp, out_file)

                    doc = {
                        'json_body': json_body
                    }

                    r = requests.post(
                        "http://127.0.0.1:8000/image",
                        json=json.dumps({
                            'data': doc
                        })
                    )
                print('Data was sent.')

            except IOError:
                print("File not accessible")

        elif not os.path.exists(args.json):
            print('Path is not correct')

    elif args.id and args.file != None:
        r = requests.post(
            "http://127.0.0.1:8000/image",
            json=json.dumps({
                'id': args.id,
                'path': args.file
            })
        )

    else:
        sys.exit("Invalid use")
