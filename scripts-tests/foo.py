#!/usr/bin/env python3
# -*- coding: utf-8 -*- 3
import pytesseract
from pytesseract import Output
import cv2
import json
import re

#imgloc = r"/home/miguel/Desktop/Project/Images/global.jpg"
imgloc = cv2.imread('global.jpg')

d = pytesseract.image_to_data(imgloc, output_type=Output.DICT)
#print(d)

arr = []
i = 0
json = {}
for key in d['text']:
    if not re.search(r'^\s$', key) and not len(key) == 0: 
        if not key in json.keys():
            json[key] = []

        json[key].append( {
            'left'  : d['left'][i],
            'top'   : d['top'][i],
            'width' : d['width'][i],
            'height': d['height'][i]
        } )
    i=i+1

print(type(json))

# boxes para o are

array = json['are']
for dic in array:
    (x, y, w, h) = (dic['left'], dic['top'], dic['width'], dic['height'])
    cv2.rectangle(imgloc, (x, y), (x + w, y + h), (0, 255, 0), 2)
                 


#n_boxes = len(d['level'])
#for i in range(n_boxes):
#    (x, y, w, h) = (d['left'][i], d['top'][i], d['width'][i], d['height'][i])
#    cv2.rectangle(imgloc, (x, y), (x + w, y + h), (0, 255, 0), 2)


cv2.imshow('img', imgloc)
cv2.waitKey(0)
