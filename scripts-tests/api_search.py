#!/usr/bin/env python3
# -*- coding: utf-8 -*- 3
from elasticsearch import Elasticsearch
from flask import Flask, request
import json

#es = Elasticsearch()
es = Elasticsearch(hosts = '127.0.0.1:9200')

#app = Flask(__name__)
      
@app.route('/<word>', methods=['GET'])
def search(word):
    # Query antiga (Estava a funcionar assim)    
    query_body = {
        "query": {
            "match":{
                "json_body": word
            }
        }
    }

    #-----------------------------------------------------
    #Teste - Match all devolve todos os documentos
    # query_body = {
    #     "query": {
    #         "match_all": { }
    #      }
    #     }
    

    res = es.search(index = "images",body = query_body)
    
    print (res)
    #print (res["hits"]["hits"])
    ids = []
    for doc in res['hits']['hits']:
        ids.append(doc)
    
    if len(ids)==0:
        return {'Error':'Not Found in any Document '}, 400

    return {'Found in Document(s) with the id(s) ':ids}, 200


@app.route('/<id>', methods = ['DELETE'])
def remove(id):
    res = es.search(index = "documents")
    ids = []
    for i in res['hits']['hits']:
        ids.append([i['_id']])
        
    if id in (item for sublist in ids for item in sublist):
        res = es.delete(index = "documents",doc_type='page',id=(id))
        return {"Status":"Document was deleted"}, 200
 
    else:
         return {"Status":"Impossible to delete, ID not found in ElasticSearch."},400

#FOR LATER
# @app.route('/update/<id>', methods = ['PUT'])
# def update(id):  
#    data = request.json['path']
#    print(data)
#    res = es.update(index = "documents",doc_type='page',id=id,body={"doc": {"path": data}})
#    return {"Status":"Document path was updated"}, 200


if __name__ == "__main__":
    app.run(debug=True, port=6000)
    
