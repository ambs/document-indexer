from PIL import Image
import os, os.path
from elasticsearch import Elasticsearch
import pytesseract as ocr

imgs = []
path = "/home/miguel/Desktop/Project/Images"

es = Elasticsearch()
i = 0
for f in os.listdir(path):

    text = ocr.image_to_string(Image.open(os.path.join(path,f)))

    doc = {
        'text' : text
    }
    result = es.index(index="test-index", id = i, body = doc)
    
    print (result['result'] + ' with id = ' + str(i))
    i = i + 1

my_query = {
    'query': {
        'match_all' : {}
    }
}
res = es.search(index="test-index", body=my_query, size=1000)
for r in res['hits']['hits']:
    print r['_source']
