from flask import Flask
from flask_restful import Api, Resource, reqparse

app = Flask(__name__)
api = Api(app)

users = [
    {
        "nome": "Miguel",
        "idade": 27,
        "ocupacao": "Trabalhador-Estudante"
    },
    {
        "nome": "Rui",
        "idade": 23,
        "ocupacao": "Mestrado/Orientador"
    },
    {
        "nome": "Alberto",
        "idade": 29,
        "ocupacao": "Lot's of stuff"
    }
]

#@app.route('/', methods=['GET'])
#def app_index():
#    return 'Hello World'

class User(Resource):
    def get(self, nome):
        for user in users:
            if(nome == user["nome"]):
                return user, 200
        return "Utilizador nao encontrado", 404

    def post(self, nome):
        parser = reqparse.RequestParser()
        parser.add_argument("idade")
        parser.add_argument("ocupacao")
        args = parser.parse_args()

        for user in users:
            if(nome == user["nome"]):
                return "Utilizador com o nome {} ja existe".format(nome), 400

        user = {
            "nome": nome,
            "idade": args["idade"],
            "ocupacao": args["ocupacao"]
        }
        users.append(user)
        return user, 201

    def put(self, nome):
        parser = reqparse.RequestParser()
        parser.add_argument("idade")
        parser.add_argument("ocupacao")
        args = parser.parse_args()

        for user in users:
            if(nome == user["nome"]):
                user["idade"] = args["idade"]
                user["ocupacao"] = args["ocupacao"]
                return user, 200
        
        user = {
            "nome": nome,
            "idade": args["idade"],
            "ocupacao": args["ocupacao"]
        }
        users.append(user)
        return user, 201

    def delete(self, nome):
        global users
        users = [user for user in users if user["nome"] != nome]
        return "{} foi apagado.".format(nome), 200
      
api.add_resource(User, "/user/<string:nome>")

app.run(host="0.0.0.0", port=3333, debug=True)
