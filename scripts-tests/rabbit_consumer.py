#!/usr/bin/env python3
# -*- coding: utf-8 -*- 3
import pika
import json

connection = pika.BlockingConnection(pika.ConnectionParameters(host = 'localhost'))

channel = connection.channel()

def callback(ch, method, properties, body):
    data = json.loads(body)
    print("[*] ID: {}".format(data['id']))
    print("[*] Path: {}".format(data['path']))


channel.basic_consume(on_message_callback = callback, queue = 'test-queue', auto_ack = True)
channel.start_consuming()