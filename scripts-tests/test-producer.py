#!/usr/bin/env python3

import requests
import json

r = requests.post(
    "http://127.0.0.1:5000/image",
    json=json.dumps({
        "id": "125_51",
        "path": "test.jpeg"
    })#,
    #headers = {'Content-type': 'application/json'}
)

print(r.status_code, r.reason)
